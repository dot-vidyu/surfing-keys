// an example to create a new mapping `ctrl-y`
// mapkey('<Ctrl-y>', 'Show me the money', function() {
//    Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
// });

// an example to replace `T` with `gt`, click `Default mappings` to see how `T` works.
// map('gt', 'T');

// an example to remove mapkey `Ctrl-i`
// unmap('<Ctrl-i>');

// Disable emoji
api.iunmap(":");

api.Hints.style('padding: 4px; border-radius: 24%; border: 0px solid #282a36AA; color: #282a36; backdrop-filter: blur(4px); background: #ffffff4a; font-size: 10pt; font-family: "Fira Code"');
api.Hints.style('border-radius: 24%; border: 0px solid #282a36AA; color: #282a36; backdrop-filter: blur(4px); background: #ffffff4a; font-size: 10pt; font-family: "Fira Code"', "text");

api.mapkey('F', 'Fullscreen', function() {
    if (document.fullscreen) {
        document.exitFullscreen();
    }
    else {
        document.documentElement.requestFullscreen();
    }
});

// set theme
settings.theme = `
.sk_theme {
  background: #ffffff8a;
  backdrop-filter: blur(4px);
  color: #282a36;
}
.sk_theme #sk_omnibarSearchResult ul li:nth-child(odd) {
  background: transparent;
}
.sk_theme #sk_omnibarSearchResult ul li.focused {
    background: #cddef984;
}
#sk_omnibarSearchResult .tab_in_window {
    box-shadow: none;
}
.sk_theme#sk_omnibar {
    border-radius: 8px;
    box-shadow: rgba(0, 0, 0, 0.3) 0px 3px 7px 0px;
}
.sk_theme#sk_keystroke {
  background: #ffffff8a;
  border-radius: 16px 16px 0 16px;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 3px 7px 0px;
  color: #282a36;
}
.sk_theme#sk_status {
    font-size: 24px;
}
.sk_theme#sk_find {
    font-size: 24px;
    background: none;
    backdrop-filter: none;
}
.sk_theme.expandRichHints span.annotation {
    color: #242464;
}

`;
// click `Save` button to make above settings to take effect.
